package week3.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class EditLead extends SeMethods{
	@Test(dependsOnMethods = {"week3.day2.TestCaseCreateLead.createLead"}, enabled = false)
	public void createLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
		
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);

		WebElement eleFindLeads = locateElement("linkText","Find Leads");
		click(eleFindLeads);

		WebElement eleFirstName = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(eleFirstName, "Thasleem");
		
		WebElement eleClickFindLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(eleClickFindLeads);
		Thread.sleep(5000);
		
		WebElement eleFirstResultingLead = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		click(eleFirstResultingLead);
		
		boolean title = verifyTitle("Find Leads | opentaps CRM");
		System.out.println(title);
		
		WebElement eleEdit = locateElement("xpath","//a[text()='Edit']");
		click(eleEdit);
		
		WebElement eleUpdateCmpnyName = locateElement("id","updateLeadForm_companyName");
		type(eleUpdateCmpnyName, "Capg");
		
		WebElement eleUpdate = locateElement("xpath","//input[@name='submitButton']");
		click(eleUpdate);
		
		WebElement verifytext = locateElement("id","viewLead_companyName_sp");
		verifyExactText(verifytext,"Capgemini");
		
		closeBrowser();
}
}
