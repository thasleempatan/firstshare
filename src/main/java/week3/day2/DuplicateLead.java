package week3.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class DuplicateLead extends SeMethods {
	@Test
	public void createLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
		
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);

		WebElement eleFindLeads = locateElement("linkText","Find Leads");
		click(eleFindLeads);
		
		WebElement eleEmail = locateElement("xpath"," //span[text()='Email']");
		click(eleEmail);
		
		WebElement eleEmailAdd = locateElement("xpath","//input[@name='emailAddress']");
		type(eleEmailAdd, "abc@gmail.com");
		
		WebElement eleFindLeads1 = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeads1);
		Thread.sleep(5000);

		WebElement eleFrstRstLead = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String text = getText(eleFrstRstLead);
		System.out.println(text);
		click(eleFrstRstLead);
		
		WebElement eleDuplicateLead = locateElement("xpath","//a[text()='Duplicate Lead']");
		click(eleDuplicateLead);
		
		verifyTitle("Duplicate Lead");
		
		WebElement eleCrtLead = locateElement("xpath","//input[@name='submitButton']");
		click(eleCrtLead);
		
		
	}
}
