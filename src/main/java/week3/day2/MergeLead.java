package week3.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class MergeLead extends SeMethods {
	@Test
	public void createLead() throws InterruptedException {
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);
		
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);
		
		WebElement eleMergeLeads = locateElement("xpath","//a[text()='Merge Leads']");
		click(eleMergeLeads);
		
		WebElement eleFromLeads = locateElement("xpath","//img[@src='/images/fieldlookup.gif']");
		click(eleFromLeads);
		
		switchToWindow(1);
		
		WebElement eleLeadId = locateElement("xpath","//input[@name='id']");
		type(eleLeadId, "105");
		
		WebElement eleFndLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFndLeads);
		Thread.sleep(5000);

		WebElement eleFrstRstLead = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String text = getText(eleFrstRstLead);
		System.out.println(text);
		click(eleFrstRstLead);		
		
		switchToWindow(0);
		
		WebElement eleToLeads = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToLeads);
		
		switchToWindow(1);

		WebElement eleLeadId1 = locateElement("xpath","//input[@name='id']");
		type(eleLeadId1, "103");
		
		WebElement eleFndLeads1 = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFndLeads1);
		Thread.sleep(5000);

		WebElement eleFrstRstLead1 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String text1 = getText(eleFrstRstLead1);
		System.out.println(text1);
		click(eleFrstRstLead1);		
		
		switchToWindow(0);
		
		WebElement eleMerge = locateElement("xpath","//a[text()='Merge']");
		click(eleMerge);
		
		acceptAlert();

		WebElement eleFL = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFL);

		WebElement eleFromLd1 = locateElement("xpath","//input[@name='id']");
		type(eleFromLd1,text);
		
		WebElement eleFindLd2 = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLd2);
		Thread.sleep(3000);

		WebElement eleErrMsg = locateElement("xpath","//div[@class='x-paging-info']");
		String text3 = getText(eleErrMsg);
		System.out.println(text3);

		closeBrowser();
}
}
