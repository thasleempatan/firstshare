package week3.day1;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import testcases.wdmethods.SeMethods;

public class ChromeLogin{

	public static void main(String[] args)throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		try {
		driver.manage().window().maximize();
	
		driver.get("http://leaftaps.com/opentaps/");
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snap/img.png");
		
		FileUtils.copyFile(src, des);
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("Capgemini");
		driver.findElementById("createLeadForm_firstName").sendKeys("Thasleem");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Patan");
		
		WebElement sourceDropDown = driver.findElementById("createLeadForm_dataSourceId");
		Select dropDown1 = new Select(sourceDropDown);
		dropDown1.selectByVisibleText("Direct Mail");
		
		WebElement mrktCmpDropDown = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown2 = new Select(mrktCmpDropDown);
		List<WebElement> l = dropDown2.getOptions();
		
		int len = l.size();
		dropDown2.selectByIndex(len-2);
		

		WebElement currcyDropDown = driver.findElementById("createLeadForm_currencyUomId");
		Select dropDown3 = new Select(currcyDropDown);
		dropDown3.selectByValue("INR");
		driver.findElementByName("submitButton").click();
		
	} catch (NoSuchElementException e) {			
		//e.printStackTrace();
		System.out.println("NoSuchElementException Thrown");
		//throw new RuntimeException();
		
	}
	finally{
		//d.close(); //closes only current tab, which was opened by Selenium
		//d.quit(); //closes whole browser
	}
	}

}
