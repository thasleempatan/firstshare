package week3.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import testcases.wdmethods.SeMethods;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public void setData() {
		testCaseName = "CreateLead";
		testCaseDesc = "Creating a new lead";
		category = "smoke";
		author = "tasleem";
	}
	
	@Test
	public void createLead() {
		
		WebElement linkCRM = locateElement("linkText","CRM/SFA");
		click(linkCRM);
		
		WebElement creatLead = locateElement("linkText","Create Lead");
		click(creatLead);
		
		WebElement cmpnyName = locateElement("id","createLeadForm_companyName");
		type(cmpnyName,"Capgemini");
		
		WebElement fullName = locateElement("id","createLeadForm_firstName");
		type(fullName,"Thasleem");
		
		WebElement lastName = locateElement("id","createLeadForm_lastName");
		type(lastName,"Patan");
	
	}

}
