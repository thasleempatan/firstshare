package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SignUpIRCTC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("tpatan");
		driver.findElementById("userRegistrationForm:password").sendKeys("Password123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Password123");
		
		WebElement securityQues = driver.findElementById("userRegistrationForm:securityQ");
		Select dropDown1 = new Select(securityQues);
		
		dropDown1.selectByVisibleText("Who was your Childhood hero?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("abcdef");
		
		WebElement prfdLang = driver.findElementById("userRegistrationForm:prelan");
		Select dropDown2 = new Select(prfdLang);
		
		dropDown2.selectByVisibleText("English");
		
		driver.findElementByXPath("//*[@id=\"userRegistrationForm:firstName\"]").sendKeys("tas");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("khan");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("patan");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		
		WebElement dayOfDOB = driver.findElementById("userRegistrationForm:dobDay");
		Select dropDown3 = new Select(dayOfDOB);
		dropDown3.selectByVisibleText("14");
		
		WebElement monthOfDOB = driver.findElementById("userRegistrationForm:dobMonth");
		Select dropDown4 = new Select(monthOfDOB);
		dropDown4.selectByVisibleText("JUL");

		WebElement yearOfDOB = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dropDown5 = new Select(yearOfDOB);
		dropDown5.selectByVisibleText("1994");

		WebElement occup = driver.findElementById("userRegistrationForm:occupation");
		Select dropDown6 = new Select(occup);
		dropDown6.selectByVisibleText("Private");
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("757626201143");
		driver.findElementById("userRegistrationForm:idno").sendKeys("abcdefghi");

		WebElement ctry = driver.findElementById("userRegistrationForm:countries");
		Select dropDown7 = new Select(ctry);
		dropDown7.selectByVisibleText("India");
	
		driver.findElementById("userRegistrationForm:email").sendKeys("ac@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8332896628");
		
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select dropDown8 = new Select(nationality);
		dropDown8.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("16-22");
		driver.findElementById("userRegistrationForm:street").sendKeys("mahindra city");
		driver.findElementById("userRegistrationForm:area").sendKeys("Chengalpattu");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603002",Keys.TAB);
		//driver.findElementById("userRegistrationForm:statesName").sendKeys("Tamilnadu");
		
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select dropDown9 = new Select(city);
		List<WebElement> l = dropDown9.getOptions();
		
		int len = l.size();
		dropDown9.selectByIndex(1);

		WebElement postOffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select dropDown10 = new Select(postOffice);
		List<WebElement> li = dropDown10.getOptions();
		
		int leng = li.size();
		dropDown10.selectByIndex(leng-1);	}
}
