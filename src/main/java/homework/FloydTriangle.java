package homework;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter no. of rows: ");
		Scanner scan = new Scanner(System.in);
		int number = scan.nextInt();
		int n=1;
		for(int i=1;i<=number; i++) {
			for(int j=1; j<=i; j++) {
				System.out.print(n+" ");
				n++;
			}
			System.out.println();
		}
	}

}
