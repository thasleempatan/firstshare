package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ProjectZoomCar extends SeMethods{
	@Test
	public void zoomCar() throws InterruptedException {
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		
		WebElement eleJrny = locateElement("linkText","Start your wonderful journey");
		click(eleJrny);
		
		WebElement eleStartPoint = locateElement("xpath","//div[@class='component-popular-locations']/div[3]");
		click(eleStartPoint);
		
		WebElement eleNext = locateElement("xpath"," //button[@class='proceed']");
		click(eleNext);
		Thread.sleep(3000);
		
		/*// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println(tomorrow);*/
		
		WebElement eleNextDay = locateElement("xpath","(//div[@class='text'])[2]");
		String tmrdate = eleNextDay.getText();
		click(eleNextDay);
		
		WebElement eleNext1 = locateElement("xpath"," //button[@class='proceed']");
		click(eleNext1);
		
		/*if((eleNextDay.getText()).equals(tomorrow)) {
			System.out.println("Start Date confirmed");
		}*/

		WebElement datechecking = locateElement("xpath","(//div[@class='days']/div)[1]");
		String dateverify = datechecking.getText();
		
		if(tmrdate.equals(dateverify)) {
			System.out.println("Date verified");
		}
		else {
			System.out.println("Date not verified");
		}
		
		
		WebElement eleDone = locateElement("xpath","//button[@class='proceed']");
		click(eleDone);
		Thread.sleep(5000);
		
		/*WebElement eleTotalcars = locateElement("xpath","//div[@class='img']");
		List<Integer> totalcars = new ArrayList<Integer>();
		totalcars.addAll(eleTotalcars);*/
		
		List<WebElement> listofcars = driver.findElementsByXPath("//button[text()='BOOK NOW']");
		System.out.println("Total Number of cars: "+ listofcars.size());
		
		List<WebElement> elepriceofcars = driver.findElementsByXPath("//div[@class='price']");
		List<Integer> priceofcars = new ArrayList<Integer>();
		for (WebElement eachcar : elepriceofcars) {
			String substr = eachcar.getText().substring(2);
			priceofcars.add(Integer.parseInt(substr));
		}
		Collections.sort(priceofcars);
		int numofcars = priceofcars.size();
		Integer highestprice = priceofcars.get(numofcars-1);
		System.out.println("Max price of car: "+highestprice);
		
		WebElement elecarname = locateElement("xpath","(//div[contains(text(),'"+highestprice+"')]/parent::div/parent::div/preceding-sibling::div)[2]/h3");
		String brandname = elecarname.getText();
		System.out.println("Name of the car with highest price : "+brandname);
		//List<WebElement> findElementsByXPath = driver.findElementsByXPath("//div[@class='details']/h3");
		
		WebElement elebookcar = locateElement("xpath","//div[contains(text(),'"+highestprice+"')]/following-sibling::button");
		click(elebookcar);
		Thread.sleep(5000);
		
		closeBrowser();
	}
}
