package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test = extent.createTest("ChromeLogin", "Create a login");
		test.assignCategory("smoke");
		test.assignAuthor("tasleem");
		
		test.pass("Browser launched successfully");
		test.pass("data demosalesmanager entered successfully");
		test.pass("Data crmsfa entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img.png").build());
		test.pass("login button is not clicked successfully");
		extent.flush();
	}
}
