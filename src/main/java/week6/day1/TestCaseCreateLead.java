package week6.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;


public class TestCaseCreateLead extends ProjectMethods{
	@BeforeTest(groups = {"common"})
	public void setData() {
		testCaseName = "Create Lead";
		testCaseDesc = "Creating a lead";
		category = "smoke";
		author = "thasleem";
		fileName = "createLead";
	}
	@Test(groups = {"smoke"}, dataProvider="dataProvider")
	public void createLead(String cName, String fName, String lName) {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
		
		WebElement eleCreateLead = locateElement("linkText","Create Lead");
		click(eleCreateLead);
		
		WebElement cmpnyName = locateElement("id","createLeadForm_companyName");
		type(cmpnyName,cName);
		
		WebElement firstName = locateElement("id","createLeadForm_firstName");
		type(firstName,fName);
		
		WebElement lastName = locateElement("id","createLeadForm_lastName");
		type(lastName,lName);

		WebElement eleLocalFirstName = locateElement("id","createLeadForm_firstNameLocal");
		type(eleLocalFirstName,"Thasleem");

		WebElement eleLocalLastName = locateElement("id","createLeadForm_lastNameLocal");
		type(eleLocalLastName,"Patan");
		
		WebElement eleSalutation = locateElement("id","createLeadForm_personalTitle");
		type(eleSalutation,"Miss");
	
		WebElement eleSourceDropDown = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSourceDropDown, "Direct Mail");
	
		WebElement eleTitle = locateElement("id","createLeadForm_generalProfTitle");
		type(eleTitle,"Associate Consultant");
		
		WebElement eleAnnualRev = locateElement("id","createLeadForm_annualRevenue");
		type(eleAnnualRev,"12");

		WebElement eleIndustry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(eleIndustry, "Computer Software");

		WebElement eleOwnerShip = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleOwnerShip, "S-Corporation");

		WebElement eleSICCOde = locateElement("id","createLeadForm_sicCode");
		type(eleSICCOde,"001");

		WebElement eleDesc = locateElement("id","createLeadForm_description");
		type(eleDesc,"Creating a new lead");

		WebElement eleImpNote = locateElement("id","createLeadForm_importantNote");
		type(eleImpNote,"Entering an important note");

		WebElement eleCntryCode = locateElement("id","createLeadForm_primaryPhoneCountryCode");
		type(eleCntryCode,"91");
		
		WebElement eleAreaCode = locateElement("id","createLeadForm_primaryPhoneAreaCode");
		type(eleAreaCode,"9");

		WebElement eleExtension = locateElement("id","createLeadForm_primaryPhoneExtension");
		type(eleExtension,"4414438");

		WebElement eleDepartment = locateElement("id","createLeadForm_departmentName");
		type(eleDepartment,"ECE");
		
		WebElement eleCurrency = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(eleCurrency, "INR - Indian Rupee");
		
		WebElement eleNoOfEmp = locateElement("id","createLeadForm_numberEmployees");
		type(eleNoOfEmp,"1000");

		WebElement eleTicker = locateElement("id","createLeadForm_tickerSymbol");
		type(eleTicker,"10");

		WebElement elePersonToAsk = locateElement("id","createLeadForm_primaryPhoneAskForName");
		type(elePersonToAsk,"tpatan");
		
		WebElement eleWebURL = locateElement("id","createLeadForm_primaryWebUrl");
		type(eleWebURL,"abcd");

		WebElement eleToName = locateElement("id","createLeadForm_generalToName");
		type(eleToName,"tasleem");

		WebElement eleAddress1 = locateElement("id","createLeadForm_generalAddress1");
		type(eleAddress1,"SEZ,Mahindra City");

		WebElement eleAddress2 = locateElement("id","createLeadForm_generalAddress2");
		type(eleAddress2,"Kancheepuram(D),Chengalpattu");

		WebElement eleCity = locateElement("id","createLeadForm_generalCity");
		type(eleCity,"Chennai");
		
		WebElement eleState = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(eleState, "Indiana");

		WebElement eleCountry = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(eleCountry, "India");

		WebElement elePostalCode = locateElement("id","createLeadForm_generalPostalCode");
		type(elePostalCode,"517325");
		
		WebElement elePostalCodeExt = locateElement("id","createLeadForm_generalPostalCodeExt");
		type(elePostalCodeExt,"01");

		WebElement eleMrktngCmpn = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleMrktngCmpn, "Car and Driver");
		
		WebElement elePhoneNum = locateElement("id","createLeadForm_primaryPhoneNumber");
		type(elePhoneNum,"8332896628");

		WebElement eleEmail = locateElement("id","createLeadForm_primaryEmail");
		type(eleEmail,"abc@gmail.com");
		
		WebElement eleCreate = locateElement("class","smallSubmit");
		click(eleCreate);

		WebElement verifytext = locateElement("id","viewLead_firstName_sp");
		verifyExactText(verifytext,"Thasleem");
		
	}
	/*@DataProvider(name ="positive")
	public Object[][] fetchData() {
		Object[][] data =  new Object[2][3];
		data[0][0] = "igate";
		data[0][1] = "tas";
		data[0][2] = "pat";
		
		data[1][0] = "infosys";
		data[1][1] = "tasleem";
		data[1][2] = "patan";
		return data;
	}*/

}
