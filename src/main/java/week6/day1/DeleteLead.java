package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class DeleteLead extends ProjectMethods{
	@BeforeTest(groups = {"common"})
	public void setData() {
		testCaseName = "Delete Lead";
		testCaseDesc = "Deleting a lead";
		category = "sanity";
		author = "thasleem";
	}
	@Test(groups = {"sanity"})
	public void createLead(String phoneNum) throws InterruptedException {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
		
		WebElement eleLeads = locateElement("linkText","Leads");
		click(eleLeads);

		WebElement eleFindLeads = locateElement("linkText","Find Leads");
		click(eleFindLeads);
		
		WebElement elePhone = locateElement("xpath","//span[text()='Phone']");
		click(elePhone);
		
		WebElement elePhoneNum = locateElement("xpath","//input[@name='phoneNumber']");
		type(elePhoneNum, phoneNum);
		
		WebElement eleButFindLeads = locateElement("xpath","//button[text()='Find Leads']");
		click(eleButFindLeads);
		Thread.sleep(5000);

		WebElement eleFrstRstLead = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		String text = getText(eleFrstRstLead);
		System.out.println(text);
		click(eleFrstRstLead);
		
		WebElement eleDelete = locateElement("xpath","//a[text()='Delete']");
		click(eleDelete);
		
		WebElement eleFindLeadsButton = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFindLeadsButton);
		
		WebElement eleLeadId = locateElement("xpath"," //input[@name='id']");
		type(eleLeadId, text);
		
		WebElement eleFindLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
		click(eleFindLeadsBtn);

		WebElement verifytext = locateElement("xpath","//div[text()='No records to display']");
		verifyPartialText(verifytext,"No records");
		
}
	/*@DataProvider(name ="deleteDataProvider")
	public Object[][] fetchData() {
		Object[] data =  new Object[1];
		data[0] = "8332896628";
		
		return data;
	}*/
}
