package pages.editlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.createlead.NewProjectMethods;
import pages.createlead.ViewLead;
import wdMethods.ProjectMethods;

public class FindLeads extends NewProjectMethods{

	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement eleclickFindLeadLink;
	public FindLeads clickFindLeadLink() {
		//WebElement eleclickFindLeadLink = locateElement("linkText","Find Leads");
		click(eleclickFindLeadLink);
		return this;
	}
	@FindBy(how = How.XPATH,using = "(//input[@name='firstName'])[3]")
	WebElement eleFirstName;
	public FindLeads typeFirstName(String data) {
		//WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, data);
		return this;
	}
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']")
	WebElement eleClickFindLeadLinkTwo;
	public FindLeads clickFindLeadLinkTwo() {
		//WebElement eleClickFindLeadLinkTwo = locateElement("xpath","//button[text()='Find Leads']");
		click(eleClickFindLeadLinkTwo);
		return this;
	}
	@FindBy(how = How.XPATH, using = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]")
	WebElement eleClickFrstRsltEle;
	public ViewLead clickFrstRsltEle() {
		//WebElement eleClickFrstRsltEle = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a[1]");
		click(eleClickFrstRsltEle);
		return new ViewLead();
	}
}
