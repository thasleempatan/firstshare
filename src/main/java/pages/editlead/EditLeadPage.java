package pages.editlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.createlead.NewProjectMethods;
import wdMethods.ProjectMethods;

public class EditLeadPage extends NewProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "eleUpdateCmpnyName")
	WebElement eleUpdateCmpnyName;
	public EditLeadPage typeUpdateCmpnyName(String data) {
		//WebElement eleUpdateCmpnyName = locateElement("id","eleUpdateCmpnyName");
		type(eleUpdateCmpnyName, data);
		return this;
	}
	@FindBy(how = How.XPATH, using = "//input[@name='submitButton']")
	WebElement eleClickUpdate;
	public FindLeads clickUpdate() {
		//WebElement eleClickUpdate = locateElement("linkText","//input[@name='submitButton']");
		click(eleClickUpdate);
		return new FindLeads();
}
	
}
