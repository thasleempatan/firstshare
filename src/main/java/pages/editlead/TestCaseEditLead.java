package pages.editlead;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.createlead.HomePage;
import pages.createlead.LoginPage;
import pages.createlead.MyHomePage;
import pages.createlead.NewProjectMethods;
import pages.createlead.ViewLead;
import wdMethods.ProjectMethods;

public class TestCaseEditLead extends NewProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "New Edit Lead";
		testCaseDesc = "Editing a lead";
		category = "smoke";
		author = "thasleem";
		fileName = "NewEditlead";
	}
	@Test(dataProvider="dataProvider")
	public void CL(String userName, String passWord, String firstName, String newCompanyName, String verifyCmpyName) throws InterruptedException {
		new LoginPage()
		.typeUserName(userName)
		.typePassword(passWord)
		.clickLogin();
		
		new HomePage()
		.clickLink();
		
		new MyHomePage()
		.clickLeadLink();
		
		new FindLeads()
		.clickFindLeadLink()
		.typeFirstName(firstName)
		.clickFindLeadLinkTwo();
		Thread.sleep(5000);
		new FindLeads()
		.clickFrstRsltEle();
		
		new pages.editlead.ViewLead()
		.clickEdit();
		
		new EditLeadPage()
		.typeUpdateCmpnyName(newCompanyName)
		.clickUpdate();
		
		new NewViewLead()
		.verifyCmpnyName(verifyCmpyName);
	}
}
