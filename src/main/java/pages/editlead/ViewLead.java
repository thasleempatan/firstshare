package pages.editlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.createlead.NewProjectMethods;
import wdMethods.ProjectMethods;

public class ViewLead extends NewProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//a[text()='Edit']")
	WebElement eleClickEdit;
	public ViewLead clickEdit() {
		//WebElement eleClickEdit = locateElement("id","password");
		click(eleClickEdit);
		return this;
	}
	
}

