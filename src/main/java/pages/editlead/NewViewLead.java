package pages.editlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pages.createlead.NewProjectMethods;
import pages.createlead.ViewLead;

public class NewViewLead extends NewProjectMethods {
	public NewViewLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "viewLead_companyName_sp")
	WebElement eleVerifyCmpnyName;
	public NewViewLead verifyCmpnyName(String data) {
		//WebElement eleVerifyCmpnyName = locateElement("id","viewLead_companyName_sp");
		verifyPartialText(eleVerifyCmpnyName, data);
		return this;
	}
	
}
