package pages.createlead;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TestCaseCL extends NewProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "New Create Lead";
		testCaseDesc = "Creating a lead";
		category = "smoke";
		author = "thasleem";
		fileName = "CL";
	}
	@Test(dataProvider="dataProvider")
	public void CL(String userName, String passWord, String cmpnyName, String firstName, String lastName, String verifyFname) {
		new LoginPage()
		.typeUserName(userName)
		.typePassword(passWord)
		.clickLogin();
		new HomePage()
		.clickLink();
		new MyHomePage()
		.clickLeadLink();
		new MyLeads()
		.clickCreateLeadLink();
		
		new CreateLead()
		.typeCmpnyName(cmpnyName)
		.typeFirstName(firstName)
		.typeLastName(lastName)
		.clickCreateLeadBut();
		
		new ViewLead()
		.verifyFirstName(verifyFname);
		
	}
}
