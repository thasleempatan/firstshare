package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends NewProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "viewLead_firstName_sp")
	WebElement eleVerifyFirstName;
	public ViewLead verifyFirstName(String data) {
		//WebElement elePassword = locateElement("id","password");
		verifyPartialText(eleVerifyFirstName, data);;
		return this;
	}
	
}

