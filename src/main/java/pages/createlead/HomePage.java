package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends NewProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//h2[text()[contains(.,'Demo Sales Manager')]]")
	WebElement eleVerifyText;
	public HomePage verifyHomePageText(String data) {
		//WebElement eleLogin = locateElement("class","decorativeSubmit");
		type(eleVerifyText, data);
		return this;
	}
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA")
	WebElement eleClickLink;
	public MyHomePage clickLink() {
		//WebElement elePassword = locateElement("id","password");
		click(eleClickLink);
		return new MyHomePage();
	}
	
}
