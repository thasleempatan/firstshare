package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomePage extends NewProjectMethods{

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using = "Leads")
	WebElement eleClickLeadLink;
	public MyLeads clickLeadLink() {
		//WebElement eleClickLeads = locateElement("linkText","Leads");
		click(eleClickLeadLink);
		return new MyLeads();
}
}
