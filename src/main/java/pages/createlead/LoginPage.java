package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class LoginPage extends NewProjectMethods{
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "username")
	WebElement eleUserName;
	public LoginPage typeUserName(String data) {
		//WebElement eleUserName = locateElement("id","username");
		type(eleUserName, data);
		return this;
	}
	@FindBy(how = How.ID, using = "password")
	WebElement elePassword;
	public LoginPage typePassword(String data) {
		//WebElement elePassword = locateElement("id","password");
		type(elePassword, data);
		return this;
	}
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogin;
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
}
