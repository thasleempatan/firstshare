package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends NewProjectMethods{

	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleClickCreateLeadLink;
	public CreateLead clickCreateLeadLink() {
		//WebElement elePassword = locateElement("linktext","Create Lead");
		click(eleClickCreateLeadLink);
		return new CreateLead();
}
}
