package pages.createlead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends NewProjectMethods{

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement eleCmpnyName;
	public CreateLead typeCmpnyName(String data) {
		//WebElement eleCmpnyName = locateElement("id","createLeadForm_companyName");
		type(eleCmpnyName, data);
		return this;
	}
	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement eleFirstName;
	public CreateLead typeFirstName(String data) {
		//WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName, data);
		return this;
	}
	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement eleLastName;
	public CreateLead typeLastName(String data) {
		//WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName, data);
		return this;
	}
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit")
	WebElement eleClickCreateLeadBut;
	public ViewLead clickCreateLeadBut() {
		//WebElement elePassword = locateElement("class","smallSubmit");
		click(eleClickCreateLeadBut);
		return new ViewLead();
}
}
