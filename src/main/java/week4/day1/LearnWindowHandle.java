package week4.day1;

import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://demo.guru99.com/popup.php");
		
		driver.findElementByLinkText("Click Here").click();
		String handle = driver.getWindowHandle();
		System.out.println(handle);
		
		Set<String> handlesWindow = driver.getWindowHandles();
		int sizeSet = handlesWindow.size();
		for (String eachWindow : handlesWindow) {
			if(!handle.equalsIgnoreCase(eachWindow)) {
				driver.switchTo().window(eachWindow);
				
				driver.findElementByXPath("/html/body/form/table/tbody/tr[5]/td[2]/input").sendKeys("abc@gmail.com");
				driver.findElementByName("btnLogin").click();
			}
		}
	}

}
